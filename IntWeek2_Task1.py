"""
1) Необходимо написать функцию калькулятор, которая принимает строку состоящую из числа, оператора и второго числа
разделенных пробелом. Например ('1 + 1'); Необходимо разделить строку используя str.split(),
и проверить является результирующий список валидным.
    a) Если ввод не состоит из 3 элементов, необходимо возбудить исключение FormulaError, которое является пользовательским
 исключением.
    b) Попытайтесь сконвертировать первое и третье значение ввода к типу float. Перехватите любые исключения типа ValueError,
 которые возникают, и выбросите FormulaError
    c) Если второе значение ввода не является '+', '-', '*', '/' также выбросите FormulaError.
Если инпут валидный - ф-я должна вернуть результат операции
"""


class FormulaError(Exception):
    def __init__(self):
        super(Exception, self).__init__()


a = input('print task: ')


def calc_func(customer_str: str):
    splited_str = customer_str.split()
    try:
        float(splited_str[0])
        float(splited_str[2])
    except ValueError:
        raise FormulaError
    if len(splited_str) != 3:
        raise FormulaError
    elif splited_str[1] not in ['+', '-', '*', '/']:
        raise FormulaError
    else:
        res = eval(customer_str)
        return res


print(calc_func(a))
