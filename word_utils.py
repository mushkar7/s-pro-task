"""
Модуль word_utils.py, позволяет:
clean_util - очистить предложение от знаков препинания;
word_list_util - получить список слов из предложения;
longest_word_util - получить самое длинное слово в предложении;
"""


def clean_util(sentence):
    return sentence.replace(',','').replace('.','').replace('?','').replace('!','').replace('-','')


def word_list_util(sentence):
    return clean_util(sentence).split(' ')


def longest_word_util(sentence):
    new_list = clean_util(sentence).split(' ')
    largest_word = ''
    for word in new_list:
        if len(word) >= len(largest_word):
            largest_word = word
    return largest_word
