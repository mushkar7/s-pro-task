# 2) Определите класс итератор ReverseIter, который принимает список и итерируется по нему в обратном направлении


class ReverseIter:
    def __init__(self, l):
        self.l = l.reverse()

    def rev_iteration(self):
        self.l = iter(l)
        while True:
            try:
                item = next(self.l)
                print(item)
            except StopIteration:
                break


l = [1,2,3,4]
rev_l = ReverseIter(l)
rev_l.rev_iteration()
