"""
Homework - Python Intermediate week1
Task 2.

2.1 Создать класс Point2D. Координаты точки задаются 2 параметрами - координатами x, y на плоскости.

2.2 Реализовать метод distance, принимает экземпляр класса Point2D и рассчитывает расстояние между точками на плоскости.

2.3 Создать защищенный атрибут класса - счетчик созданных экземпляров класса.

2.4 Чтение количества экземпляров реализовать через метод getter.

2.5 Создать класс Point3D, наследник класса Point2D. Координаты точки задаются 3 параметрами - x, y, z.

2.6 Переопределить конструктор с помощью super().

2.7 Переопределить метод distance для определения расстояния между 2-мя точками в пространстве.

Загрузить задание на git репозиторий, и прикрепить линк на него как ответ
"""


class Point2D:
    _point_counter = 0

    # 2.3 Защищенный атрибут класса - счетчик созданных экземпляров класса.

    def __init__(self, x, y):
        self.x = x
        self.y = y
        Point2D._point_counter += 1
    # 2.1 Создать класс Point2D. Координаты точки задаются 2 параметрами - координатами x, y на плоскости.

    def distance(self, point2):
        x1 = self.x
        y1 = self.y
        x2 = point2.x
        y2 = point2.y
        xd = x1 - x2
        yd = y1 - y2
        dist = (xd ** 2 + yd ** 2) ** 0.5
        return dist
    #  2.2 Метод distance, принимает экземпляр класса Point2D и рассчитывает расстояние между точками на плоскости.

    @property
    def get_counter(self):
        return self._point_counter
    # 2.4 Чтение количества экземпляров класса через метод getter.


class Point3D(Point2D):

    def __init__(self, x, y, z):
        super(Point3D, self).__init__(x, y)
        # 2.6 Переопределить конструктор с помощью super().
        self.z = z
    # 2.5 Класс Point3D, наследник класса Point2D. Координаты точки задаются 3 параметрами - x, y, z.

    def distance(self, point2):
        x1 = self.x
        y1 = self.y
        z1 = self.z
        x2 = point2.x
        y2 = point2.y
        z2 = point2.z
        xd = x1 - x2
        yd = y1 - y2
        zd = z1 - z2
        dist = (xd ** 2 + yd ** 2) ** 0.5
        dist3D = (zd ** 2 + dist ** 2) ** 0.5
        return dist3D
    #


point1 = Point2D(10, 20)
point2 = Point2D(-10, -20)
print(Point2D.distance(point2, point1))  # 2.2

print(point1.get_counter)  # 2.4

point3D1 = Point3D(10, 20, 10)
point3D2 = Point3D(-10, -20, -10)
print(Point3D.distance(point3D2, point3D1))  # 2.7
