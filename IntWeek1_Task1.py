"""
Homework - Python Intermediate week1
Task 1.

1.1 Создать класс дробь(Fraction), конструктор которого принимает целые числа
(num - числитель(numerator), den - знаменатель(denominator) ).
1.2 Атрибуты числитель и знаменатель в классе сделать приватными.
1.3 Доступ к атрибутам реализовать через свойства.
1.4 Переопределить методы __sub__, __add__, __mull__, __truediv__ для того,
чтобы можно было выполнять соответствующие математические действия с объектами класса дробь.
(Вычитание, сложение, умножение и деление).
1.5 Добавить класс миксин, в котором реализовать статические методы, для этих же операций(add, sub, mull, div).
Добавить класс миксин в класс Fraction.
1.6 Создать classmethod который из строки типа 'числитель/знаменатель' возвращает объект класса дробь.
1.7 Переопределить метод __str__, который при выводе объекта на печать будет выводить строку вида num / den.
1.8 Создать объекты класса дробь.
1.9 Выполнить все реализованные методы.
"""


class Mixin:  # 1.5

    @staticmethod
    def sub(self, other):
        fr1 = self.get_num() / self.get_den()
        fr2 = other.get_num() / other.get_den()
        fr_res = fr1 - fr2
        conv_num = round(fr_res * self.get_den() * other.get_den())
        conv_den = self.get_den() * other.get_den()
        return Fraction(conv_num, conv_den)

    @staticmethod
    def add(self, other):
        fr1 = self.get_num() / self.get_den()
        fr2 = other.get_num() / other.get_den()
        fr_res = fr1 + fr2
        conv_num = round(fr_res * self.get_den() * other.get_den())
        conv_den = self.get_den() * other.get_den()
        return Fraction(conv_num, conv_den)

    @staticmethod
    def mull(self, other):
        fr1 = self.get_num() / self.get_den()
        fr2 = other.get_num() / other.get_den()
        fr_res = fr1 * fr2
        conv_num = round(fr_res * self.get_den() * other.get_den())
        conv_den = self.get_den() * other.get_den()
        return Fraction(conv_num, conv_den)

    @staticmethod
    def div(self, other):
        fr1 = self.get_num() / self.get_den()
        fr2 = other.get_num() / other.get_den()
        fr_res = fr1 / fr2
        conv_num = round(fr_res * self.get_den() * other.get_den())
        conv_den = self.get_den() * other.get_den()
        return Fraction(conv_num, conv_den)
# 1.5 Добавить класс миксин, в котором реализовать статические методы, для этих же операций(add, sub, mull, div).


class Fraction(Mixin):
    # 1.1 Создать класс дробь(Fraction), конструктор которого принимает целые числа
    # (num - числитель(numerator), den - знаменатель(denominator) ).
    def __init__(self, num, den):
        self.__num = num
        self.__den = den
        # 1.2 Атрибуты числитель и знаменатель в классе сделать приватными.

    def __str__(self):
        return f'{self.__num} / {self.__den}'

    # 1.7 Метод при выводе объекта на печать выводит строку вида num / den.

    def get_num(self):
        return self.__num

    def get_den(self):
        return self.__den

    def set_num(self, value):
        self.__num = value

    def set_den(self, value):
        self.__den = value
    # 1.3 Доступ к атрибутам реализовать через свойства.

    def __sub__(self, other):
        sub_den = self.__den * other.__den
        sub_num = self.__num * other.__den - other.__num * self.__den
        return Fraction(sub_num, sub_den)

    def __add__(self, other):
        add_den = self.__den * other.__den
        add_num = self.__num * other.__den + other.__num * self.__den
        return Fraction(add_num, add_den)

    def __mull__(self, other):
        mull_den = self.__den * other.__den
        mull_num = self.__num * other.__num
        return Fraction(mull_num, mull_den)

    def __truediv__(self, other):
        truediv_den = self.__den * other.__num
        truediv_num = self.__num * other.__den
        return Fraction(truediv_num, truediv_den)
    # 1.4 Переопределить методы __sub__, __add__, __mull__, __truediv__ для того,
    # Чтобы можно было выполнять соответствующие математические действия с объектами класса дробь.
    # (Вычитание, сложение, умножение и деление).

    @classmethod
    def string_to_fraction(cls, str_frac):
        numb_list = str_frac.split('/')
        return Fraction(int(numb_list[0]), int(numb_list[1]))
# 1.6 Метод возвращает из строки типа 'числитель/знаменатель' объект класса дробь.




fraction1 = Fraction(4, 5)
fraction2 = Fraction(1, 2)
# 1.8 Создать объекты класса дробь.
fraction1.set_num(2)
fraction1.set_den(3)
print(fraction1.get_num(), fraction1.get_den())
# 1.3
#
print(fraction1.__str__())
print(fraction2.__str__())
# 1.7
fraction3 = fraction1.__sub__(fraction2)
print(fraction3.__str__())  # 4/6 - 3/6 = 1/6

fraction4 = fraction1.__add__(fraction2)
print(fraction4.__str__())  # 4/6 + 3/6 = 7/6

fraction5 = fraction1.__mull__(fraction2)
print(fraction5.__str__())  # 2/3 * 1/2 = 2/6

fraction6 = fraction1.__truediv__(fraction2)
print(fraction6.__str__())  # 2/3 / 1/2 = 4/3
# 1.4

print((Mixin.sub(fraction1, fraction2)))
print((Mixin.add(fraction1, fraction2)))
print((Mixin.mull(fraction1, fraction2)))
print((Mixin.div(fraction1, fraction2)))
# 1.5

s = '5/8'
print(Fraction.string_to_fraction(s))  # 1.6
