"""
3) Определить функцию-генератор fib_generator(n), которая принимает количество элементов последовательности Фибоначчи и
итерируется по элементам последовательности. например fib_generator(3) создаст итератор для 3 элементов последовательности 0 1 1

Чи́сла Фибона́ччи — элементы числовой последовательности 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89, 144, 233, 377, 610, 987,
 1597, 2584, 4181, 6765, 10946, 17711, …, в которой первые два числа равны 0 и 1, а каждое последующее число равно сумме
 двух предыдущих чисел

Написать подобную ф-ю fib_list(n) которая возвращает список с элементами последовательности.

Вызов ф-и fib_list(3) вернет список [0, 1, 1]
"""


def fib_generator(n):
    for i in range(1,(n+1)):
        print(fibonacci(i))


def fib_list(n):
    fibonacci_list = []
    for i in range(1,(n+1)):
        fibonacci_list.append(fibonacci(i))
    return fibonacci_list


def fibonacci(n):
    cache = ()
    if n in cache:
        return cache[n]
    if n == 1:
        x = 0
    elif n == 2:
        x = 1
    else:
        x = fibonacci(n-1) + fibonacci(n-2)
    return x


fib_generator(3)

print(fib_list(3))
