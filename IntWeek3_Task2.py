"""
2) Реализуйте модуль  word_utils.py, позволяющий:
- очистить предложение от знаков препинания;
- получить список слов из предложения;
- получить самое длинное слово в предложении;
"""
from word_utils import *

sen = 'What have you done, buddy?'

print(clean_util(sen))

print(word_list_util(sen))

print(longest_word_util(sen))